# RNAseq Snakemake pipeline for Paired-end Illumina sequencing using a Reference genome
# Pipeline features:
# 1) read preprocessing,
# 2) reads QC,
# 3) mapping with STAR using 2pass method,
# 4) mapping QC
# 5) SNP calling using GATK
# Counts table directly usable in the dicoexpress R pipeline.
# Snakemake features: fastq from csv file, config, modules, SLURM


import pandas as pd
import os
import sys
from snakemake.utils import min_version
# snakemake built-in report function requires min version 5.1
min_version("5.1.0")

#read the sample file using pandas lib (sample names+ fastq names) and crezate index using the sample name
samples = pd.read_csv(config["samplesfile"], sep='\t', dtype=str, comment='#').set_index(["SampleName"], drop=False)


# Build a chr list based on GENOME_FAI
#CHRS=list()
#with open(config["REFPATH"]+"/"+config["GENOME_FAI"],'r') as fh:
#    #line = fh.readline()
#    for line in fh:
#      line=line.strip()
#      fs=re.split(r'\t',line)
#      CHRS.append(fs[0])
#      #CHRS.append(line)
#fh.close()


# Read the sample file using pandas lib (sample names+ fastq names) and create index using the sample name
samples = pd.read_csv(config["samplesfile"], sep='\t', dtype=str, comment='#').set_index(["SampleName"], drop=False)


# Build a chr list based on GENOME_FAI
CHRS=list()
with open(config["REFPATH"]+"/"+config["GENOME_FAI"],'r') as fh:
    #line = fh.readline()
    for line in fh:
      line=line.strip()
      fs=re.split(r'\t',line)
      CHRS.append(fs[0])
      #CHRS.append(line)
fh.close()


# Get fastq R1. Could have several fastq file for 1 sample
def get_fql1(wildcards):
    tt=samples.loc[(wildcards.sample), ["fq1"]].dropna()
    fs=re.split('[ ;,]+',tt.item())
    ml=list()
    for f in fs:
      f.strip()
      ml.append(config["fq_dir"]+"/"+f)
    return ml


# Get fastq R2. Could have several fastq file for 1 sample
def get_fql2(wildcards):
    tt=samples.loc[(wildcards.sample), ["fq2"]].dropna()
    fs=re.split('[ ;,]+',tt.item())
    ml=list()
    for f in fs:
      f.strip()
      ml.append(config["fq_dir"]+"/"+f)
    return ml


############################################### Final rule #############################################
rule final_outs:
    input:
        "{outdir}/multiqc/multiqc_report_fastqc.html".format(outdir=config["outdir"]),
        "{outdir}/multiqc/multiqc_report_bam.html".format(outdir=config["outdir"]),
        #expand("{outdir}/mapped/starpass2/{sample}/Aligned.sortedByCoord.out.bam", outdir=config["outdir"], sample=samples['SampleName']),
        #expand("{outdir}/variant/gatk_gvcf/{sample}-{mychr}.g.vcf.gz",outdir=config["outdir"], sample=samples['SampleName'], mychr=CHRS),
        #expand("{outdir}/variant/gatk_genomicsdb_{mychr}.ok",outdir=config["outdir"], mychr=CHRS),
        #expand("{outdir}/variant/gatk_{mychr}_genotyped.vcf.gz",outdir=config["outdir"], mychr=CHRS),
        #"{outdir}/variant/gatk_all.vcf.gz".format(outdir=config["outdir"]),
        "{outdir}/variant/gatk_all.score_snps.pdf".format(outdir=config["outdir"]),
        "{outdir}/variant/gatk_all.score_indels.pdf".format(outdir=config["outdir"])



##################################################################################
########################  PREPROCESSING  #########################################
##################################################################################
#module 4 fastp for Paired-end read
#use with utils.smk
# 2alternatives preprocessing
rule fastp_pe:
    input:
        R1=get_fql1,
        R2=get_fql2,
    output:
        R1 = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
        R2 = "{outdir}/fastp/{{sample}}_2_trim.fastq.gz".format(outdir=config["outdir"])
    message: "Running fastp on files {input.R1} and {input.R2} \n"
    params:
        fqdir      = config["fq_dir"],
        outdir     = config["outdir"],
        modules    = config["MODULES"],
        fastp_bin  = config["fastp_bin"],
        bind       = config["BIND"],
        json       = config["outdir"]+"/fastp/{sample}_trim.json",
        html       = config["outdir"]+"/fastp/{sample}_trim.html"
    shell:
       """
        singularity exec {params.bind} {params.fastp_bin} fastp \
        --stdin \
        -i <(zcat {input.R1}) \
        -I <(zcat {input.R2}) \
        -o {output.R1} \
        -O {output.R2} \
        -h {params.html} \
        -j {params.json} \
        --max_len1 350 \
        --correction \
        --cut_mean_quality 20 \
        --cut_window_size 4 \
        --low_complexity_filter \
        --complexity_threshold 30 \
        -w 4
        rm -f {params.html} {params.json}
        exit 0

        singularity exec {params.bind} {params.fastp_bin} fastp \
        -i {input.R1} \
        -I {input.R2} \
        -o {output.R1} \
        -O {output.R2} \
        --json={params.json} \
        --html={params.html} \
        --trim_tail1=1 \
        --cut_front \
        --cut_tail \
        --length_required 35 \
        --average_qual 20 \
        --length_limit 400 \
        --correction \
        --cut_mean_quality 10 \
        --low_complexity_filter \
        --complexity_threshold 20 \
        --thread 4
        #rm -f {params.html} {params.json}
        """

#          singularity exec {params.bind} {params.fastp_bin} fastp \
#         -i {input.R1} \
#         -I {input.R2} \
#         -o {output.R1} \
#         -O {output.R2} \
#         --json={params.json} \
#         --html={params.html} \
#         --trim_tail1=1 \
#         --cut_front \
#         --cut_tail \
#         --length_required 35 \
#         --average_qual 10 \
#         --length_limit 400 \
#         --correction \
#         --cut_mean_quality 10 \
#         --low_complexity_filter \
#         --complexity_threshold 20 \
#         --thread 4
#         #rm -f {params.html} {params.json}

#QC
#fastqc PE mode
#function return fastq1 and fastq2 files and adds the fastq path
rule fastqc_pe:
    input:
        R1 = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
        R2 = "{outdir}/fastp/{{sample}}_2_trim.fastq.gz".format(outdir=config["outdir"])
    output:
        # to avoid the output name generated by fastqc (regex on the name) we use a flag file
        "{outdir}/fastqc/{{sample}}.OK.done".format(outdir=config["outdir"])
    threads:
        2
    resources:
        mem_mb=4000
    params:
        fqdir      = config["fq_dir"],
        outdir     = config["outdir"],
        fastqc_bin = config["fastqc_bin"],
        bind       = config["BIND"]
    shell:
        """
        #mkdir -p {params.outdir}/fastqc #snakemake create automaticly the folders
        singularity exec {params.bind} {params.fastqc_bin} fastqc -o {params.outdir}/fastqc -t {threads} {input.R1} {input.R2} && touch {output}
        """
#QC
# multiQC on fastqc outputs
rule multiqc_fastqc:
    input:
        expand("{outdir}/fastqc/{sample}.OK.done", outdir=config["outdir"], sample=samples['SampleName'])
    output:
        #report("{outdir}/multiqc/multiqc_report_fastqc.html".format(outdir=config["outdir"]), caption="report/multiqc.rst", category="Quality control")
        "{outdir}/multiqc/multiqc_report_fastqc.html".format(outdir=config["outdir"])
    threads:
        1
    params:
        outdir      = config["outdir"],
        multiqc_bin = config["multiqc_bin"],
        bind        = config["BIND"]
    shell:
        """
        #mkdir -p {params.outdir}/multiqc #snakemake create automaticly the folders
        singularity exec {params.bind} {params.multiqc_bin} multiqc --filename {output} {params.outdir}/fastqc
        """
#rules for STAR mapper using 2 pass
#based on:
#https://evodify.com/rna-seq-star-snakemake/
# gff3 must be converted to gtf:
#singularity exec /nosave/project/gafl/tools/containers/cufflinks_v2.2.1.sif gffread Annuum.v.2.0.gff3 -T -o Annuum.CM334.v1.6.Total.gtf
rule star_index:
        input:
            genome  = config["REFPATH"] + "/" + config["GENOME"], # provide your reference FASTA file
            gtf = config["REFPATH"] + "/" + config["GTF"]     # provide your GTF file
        output:
            "{refpath}/SAindex".format(refpath=config["REFPATH"]),
            "{refpath}/indexing.OK".format(refpath=config["REFPATH"])
        threads: 20 # set the maximum number of available cores
        params:
            outdir    = config["REFPATH"],
            bind       = config["BIND"],
            star_bin  = config["star_bin"]
        shell:
            """
            cd {params.outdir} && \
            singularity exec {params.bind} {params.star_bin} STAR --runThreadN {threads} \
            --runMode genomeGenerate \
            --genomeDir {params.outdir} \
            --genomeFastaFiles {input.genome} \
            --sjdbGTFfile {input.gtf} \
            --sjdbOverhang 100 && \
            touch {output}
            """

rule star_pass1:
        input:
            R1 = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
            R2 = "{outdir}/fastp/{{sample}}_2_trim.fastq.gz".format(outdir=config["outdir"]),
            refindex="{refpath}/indexing.OK".format(refpath=config["REFPATH"])
        params:
            bind      = config["BIND"],
            refdir    = config["REFPATH"],
            outdir    = "{outdir}/mapped/starpass1/{{sample}}".format(outdir=config["outdir"]),
            rmbam     = "{outdir}/mapped/starpass1/{{sample}}/Aligned.out.bam".format(outdir=config["outdir"]),
            star_bin  = config["star_bin"]
        output:
            sj      = "{outdir}/mapped/starpass1/{{sample}}/SJ.out.tab".format(outdir=config["outdir"]),
            pass1ok = "{outdir}/mapped/starpass1/{{sample}}/pass1.OK".format(outdir=config["outdir"])
        threads: 10
        shell:
            """
            mkdir -p {params.outdir} && cd {params.outdir} && \
            singularity exec {params.bind} {params.star_bin} STAR --runThreadN {threads} \
            --peOverlapNbasesMin 30 \
            --peOverlapMMp 0.03 \
            --genomeDir {params.refdir} \
            --readFilesIn {input.R1} {input.R2} \
            --readFilesCommand zcat \
            --outSAMtype None && \
            touch {output.pass1ok}
            rm -f {params.rmbam}
            #--outSAMtype BAM Unsorted #if we keep the output bam
            """

###################### filter and build SJ file for each sample ###########################
#rule star_sjdir:
#        output: "{outdir}/mapped/SJ".format(outdir=config["outdir"])
#        threads: 1
#        shell: 'mkdir -p {output}'
rule star_sjfilter:
        input:
            sj       = "{outdir}/mapped/starpass1/{{sample}}/SJ.out.tab".format(outdir=config["outdir"]),
            pass1ok  = "{outdir}/mapped/starpass1/{{sample}}/pass1.OK".format(outdir=config["outdir"]),
            #sjpath   = "{outdir}/mapped/SJ".format(outdir=config["outdir"])
        output:
            sjfiltered  = "{outdir}/mapped/SJ/{{sample}}SJ.filtered.tab".format(outdir=config["outdir"])
        params:
            bind      = config["BIND"],
            star_bin  = config["star_bin"] # to use internal awk,cut and sort
        threads: 1
        shell:
            """
            singularity exec {params.bind} {params.star_bin} cat {input.sj} | \
            singularity exec {params.bind} {params.star_bin} awk '($5 > 0 && $7 > 3 && $6==0)' | \
            singularity exec {params.bind} {params.star_bin} cut -f1-6 | \
            singularity exec {params.bind} {params.star_bin} sort | \
            singularity exec {params.bind} {params.star_bin} uniq > {output.sjfiltered}
            # a simple filter
            #singularity exec {params.bind} {params.star_bin} awk "{{ if (\$7 >= 3) print \$0 }}" {input[0]} > {input[0]}.filtered && \
            #mv {input[0]}.filtered {output.sjfiltered}
            """

##################### filter and build one SJ file ####################################
rule sj_filter:
    input:
       lambda wildcards: expand(expand("{outdir}/mapped/starpass1/{sample}/SJ.out.tab", outdir=config["outdir"], sample=samples['SampleName']))
    output:
        sjfilmerged = "{outdir}/mapped/starpass1/SJ.filtered.merged.tab".format(outdir=config["outdir"])
    params:
        sjpath  = "{outdir}/mapped/starpass1".format(outdir=config["outdir"]),
        sjlist = "{outdir}/mapped/starpass1/*/SJ.out.tab".format(outdir=config["outdir"])
    shell:
        """
        cat {params.sjlist} | \
        singularity exec {params.bind} {params.star_bin} awk '($5 > 0 && $7 > 2 && $6==0)' | \
        singularity exec {params.bind} {params.star_bin} cut -f1-6 | \
        singularity exec {params.bind} {params.star_bin} sort | \
        singularity exec {params.bind} {params.star_bin} uniq > {output}
        """

rule star_pass2:
        input:
            R1        = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
            R2        = "{outdir}/fastp/{{sample}}_2_trim.fastq.gz".format(outdir=config["outdir"]),
            refindex  = "{refpath}/indexing.OK".format(refpath=config["REFPATH"]),
            sjs       = lambda wildcards: expand(expand("{outdir}/mapped/SJ/{sample}SJ.filtered.tab", outdir=config["outdir"], sample=samples['SampleName'])),
            #sjfilmerged = "{outdir}/mapped/starpass1/SJ.filtered.merged.tab".format(outdir=config["outdir"])
        output:
            bam       = "{outdir}/mapped/starpass2/{{sample}}/Aligned.sortedByCoord.out.bam".format(outdir=config["outdir"]),
            genecount = "{outdir}/mapped/starpass2/{{sample}}/ReadsPerGene.out.tab".format(outdir=config["outdir"]),
            pass2ok   = "{outdir}/mapped/starpass2/{{sample}}/pass2.OK".format(outdir=config["outdir"])
        params:
            refdir       = config["REFPATH"],
            outdir       = "{outdir}/mapped/starpass2/{{sample}}".format(outdir=config["outdir"]),
            id           = "{sample}",
            samtools_bin = config["samtools_bin"],
            bind         = config["BIND"],
            star_bin     = config["star_bin"]
        threads: 10 # set the cores
        message:
            "STAR pass 2\n"
        shell:
           """
           mkdir -p {params.outdir} && cd {params.outdir} && \
           singularity exec {params.bind} {params.star_bin} STAR --runThreadN {threads} \
           --peOverlapNbasesMin 30 \
           --peOverlapMMp 0.03 \
           --genomeDir {params.refdir} \
           --readFilesIn {input.R1} {input.R2} \
           --readFilesCommand zcat \
           --outSAMtype BAM SortedByCoordinate \
           --sjdbFileChrStartEnd {input.sjs} \
           --outSAMattrRGline ID:{params.id} LB:lib1 PL:ILLUMINA PU:unit1 SM:{params.id} \
           --quantMode GeneCounts && \
           touch {params.outdir}/pass2.OK
           # singularity exec {params.bind} {params.samtools_bin} samtools index -@ {threads} {output.bam}
           # --quantMode TranscriptomeSAM GeneCounts && \
           # the filtered and merged SJ sjfilmerged could be use instead the SJ list
           """

rule bam_stats:
    input:
        bam       = "{outdir}/mapped/{{sample}}_SplitNCigar.bam".format(outdir=config["outdir"])
    output:
        stats = "{outdir}/mapped/{{sample}}.stats.txt".format(outdir=config["outdir"])
    params:
        outdir       = config["outdir"],
        idxbase      = config["REFPATH"] + "/" + config["GENOME"],
        bind         = config["BIND"],
        samtools_bin = config["samtools_bin"]
    threads: 2
    shell:
        """
        singularity exec {params.bind} {params.samtools_bin} samtools stats -@ 2 {input.bam} > {output.stats}
        """


# 2-5) multiqc for bams
rule multiqc_bam:
    input:
        expand("{outdir}/mapped/{sample}.stats.txt", outdir=config["outdir"], sample=samples['SampleName'])
    output:
        "{outdir}/multiqc/multiqc_report_bam.html".format(outdir=config["outdir"])
        #"{outdir}/multiqc/multiqc_report.html".format(outdir=config["outdir"])
    threads:
        1
    params:
        outdir      = config["outdir"]+"/mapped/*.stats.txt",
        multiqc_bin = config["multiqc_bin"],
        bind        = config["BIND"]
    shell:
        """
        singularity exec {params.bind} {params.multiqc_bin} multiqc --filename {output} {input}
        """

################################################################################
###########################  SNP calling using  GATK ###########################
################################################################################

# bam preparation for GATK:
# https://gatk.broadinstitute.org/hc/en-us/articles/360035531192-RNAseq-short-variant-discovery-SNPs-Indels-
rule MarkDuplicates:
    input:
        bam       = "{outdir}/mapped/starpass2/{{sample}}/Aligned.sortedByCoord.out.bam".format(outdir=config["outdir"]),
        pass2ok   = "{outdir}/mapped/starpass2/{{sample}}/pass2.OK".format(outdir=config["outdir"])
    output:
        bam       = "{outdir}/mapped/{{sample}}_markduplicates.bam".format(outdir=config["outdir"]),
        metrics   = "{outdir}/mapped/{{sample}}_marked_dup_metrics.txt".format(outdir=config["outdir"])
    params:
        bind       = config["BIND"],
        picard_bin = config["picard_bin"],
    threads: 1 # set the # cores
    shell:
        """
        singularity exec {params.bind} {params.picard_bin} picard MarkDuplicates \
        I={input.bam} \
        O={output.bam} \
        M={output.metrics} && rm -f {input.bam}*
        """


rule SplitNCigarReads:
    input:
        bam       = "{outdir}/mapped/{{sample}}_markduplicates.bam".format(outdir=config["outdir"]),
        genome  = config["REFPATH"] + "/" + config["GENOME"],
    output:
        bam       = "{outdir}/mapped/{{sample}}_SplitNCigar.bam".format(outdir=config["outdir"]),
        bai       = "{outdir}/mapped/{{sample}}_SplitNCigar.bai".format(outdir=config["outdir"]),
    params:
        bind = config["BIND"],
        tempdir=config["outdir"] + "/variant/tmpgatkdir",
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 1 # set the #cores
    shell:
        """
        mkdir -p {params.tempdir}
        singularity exec {params.bind} {params.gatk4_bin} gatk SplitNCigarReads \
        --tmp-dir={params.tempdir} \
        -R {input.genome} \
        -I {input.bam} \
        -O {output.bam} && rm -f {input.bam}*
        """

# 3) SNP caller using GATK4
#gatk:4 rules
# 3-1) create a reference dictionnary the ref must not be a sl and a ref.fa => ref.dict (not ref.fa.dict)
rule gatk4_ref_dict:
    input:
        ref  = "{refp}/{ref}".format(refp=config["REFPATH"],ref=config["GENOME"]),
        fai  = "{refp}/{ref}".format(refp=config["REFPATH"],ref=config["GENOME_FAI"]),
    output:
        dic  = "{refp}/{ref}.dict".format(refp=config["REFPATH"],ref=config["GENOME"]) #config["REFPATH"] + "/" + config["GENOME"] +".dict"
    params:
        refp = config["REFPATH"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    shell:
        """
        rm -f {params.refp}/*.dict
        singularity exec {params.bind} {params.gatk4_bin} gatk CreateSequenceDictionary -R {input.ref}
        #singularity exec {params.bind} {params.samtools_bin} samtools faidx {input.ref}
        singularity exec {params.bind} {params.samtools_bin} samtools dict -o {output.dic} {input.ref}
        """

# ------------------  parrallel by chr -----------------
# 3-2) HaplotypeCaller for each sample and each chr
rule gatk4_hc:
    input:
        #"{outdir}/variant/chrslist.OK".format(outdir=config["outdir"]),
        bam       = "{outdir}/mapped/{{sample}}_SplitNCigar.bam".format(outdir=config["outdir"]),
        bai    = "{outdir}/mapped/{{sample}}_SplitNCigar.bai".format(outdir=config["outdir"]),
        refdict = "{refp}/{ref}.dict".format(refp=config["REFPATH"],ref=config["GENOME"]),
    output:
        gvcf="{outdir}/variant/gatk_gvcf/{{sample}}-{{mychr}}.g.vcf.gz".format(outdir=config["outdir"]),
        gtbi="{outdir}/variant/gatk_gvcf/{{sample}}-{{mychr}}.g.vcf.gz.tbi".format(outdir=config["outdir"])
    params:
        ch="{mychr}",
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 1
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx8G -XX:ParallelGCThreads={threads}' HaplotypeCaller \
        --reference {params.ref} \
        --input {input.bam} \
        -ERC GVCF -L {params.ch} \
        --output {output.gvcf}
        exit 0
        """

# 3-3) Generate a map (text file) of gvcf files for each chr
rule gatk4_gvcf_map_file:
    input:
        gvcfs = expand("{outdir}/variant/gatk_gvcf/{sample}-{{mychr}}.g.vcf.gz", outdir=config["outdir"], sample=samples['SampleName']),
        gtbi  = expand("{outdir}/variant/gatk_gvcf/{sample}-{{mychr}}.g.vcf.gz.tbi", outdir=config["outdir"], sample=samples['SampleName']),
        fai  = "{refp}/{ref}".format(refp=config["REFPATH"],ref=config["GENOME_FAI"]),
        refdict = "{refp}/{ref}.dict".format(refp=config["REFPATH"],ref=config["GENOME"]),
    output:
        gvcfmap      = "{outdir}/variant/gvcf_{{mychr}}_list.map".format(outdir=config["outdir"]),
    params:
        ch="{mychr}",
        csv         = expand("{sample}\t{outdir}/variant/gatk_gvcf/{sample}", outdir=config["outdir"], sample=samples['SampleName']),
        bamsp       = "{outdir}/mapped".format(outdir=config["outdir"]),
        outlist     = config["outdir"]
    threads: 1
    run:
        x=params.csv
        f = open(output.gvcfmap, "w")
        for i in x:
            f.write("{}-{}.g.vcf.gz\n".format(i,params.ch))
        f.close()


# 3-4) build a datastore of gvcf for each chr
# see: https://gatk.broadinstitute.org/hc/en-us/articles/360040096732-GenomicsDBImport
rule gatk4_gdb:
    input:
        gvcf = expand("{outdir}/variant/gatk_gvcf/{sample}-{{mychr}}.g.vcf.gz",outdir=config["outdir"],sample=samples['SampleName']),
        gtbi = expand("{outdir}/variant/gatk_gvcf/{sample}-{{mychr}}.g.vcf.gz.tbi",outdir=config["outdir"],sample=samples['SampleName']),
        gvcfmap      = "{outdir}/variant/gvcf_{{mychr}}_list.map".format(outdir=config["outdir"]),
    output:
        "{outdir}/variant/gatk_genomicsdb_{{mychr}}.ok".format(outdir=config["outdir"])
    params:
        ch="{mychr}",
        tempdir=config["outdir"] + "/variant/tmpgatkdir",
        gdb  = config["outdir"] + "/variant/" + "GenomicsDB_{mychr}",
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 1
    shell:
        """
        mkdir -p {params.tempdir}
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx16g -Xms16g' GenomicsDBImport \
        --genomicsdb-workspace-path {params.gdb} \
        --batch-size 200 \
        -L {params.ch} \
        --sample-name-map {input.gvcfmap} \
        --reader-threads {threads} \
        --tmp-dir={params.tempdir} && touch {output}
        """


# 3-5) genotype (GenotypeGVCFs) for all samples for each chr
# https://gatk.broadinstitute.org/hc/en-us/articles/360047218551-GenotypeGVCFs
rule gatk4_gc:
    input:
        gdb="{outdir}/variant/gatk_genomicsdb_{{mychr}}.ok".format(outdir=config["outdir"])
    output:
        vcf="{outdir}/variant/gatk_{{mychr}}_genotyped.vcf.gz".format(outdir=config["outdir"]),
        tbi="{outdir}/variant/gatk_{{mychr}}_genotyped.vcf.gz.tbi".format(outdir=config["outdir"])
        # WARNING if {outdir}/variant/gatk_{{mychr}}.vcf.gz is NOT working we need {{mychr}}_something
    params:
        tempdir=config["outdir"] + "/tmpgatkdir",
        gdb  = config["outdir"] + "/variant/" + "GenomicsDB_{mychr}",
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 1
    message: "GATK4 genotype_variants vcf\n"
    shell:
        """
        mkdir -p {params.tempdir}
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx12G -XX:ParallelGCThreads={threads}' GenotypeGVCFs \
        --variant gendb://{params.gdb} \
        --reference {params.ref} \
        --output {output.vcf} \
        --tmp-dir={params.tempdir}
        """

# 3-6) merge all the vcf produced by chr
rule combinevcf:
    input:
        #"{outdir}/variant/chrslist.OK".format(outdir=config["outdir"]),
        vcf=expand("{outdir}/variant/gatk_{mychr}_genotyped.vcf.gz",outdir=config["outdir"],mychr=CHRS),
        tbi=expand("{outdir}/variant/gatk_{mychr}_genotyped.vcf.gz.tbi",outdir=config["outdir"],mychr=CHRS),
        refdict = config["REFPATH"] + "/" + config["GENOME"] +".dict",
    output:
        vcf = "{outdir}/variant/gatk_all.vcf.gz".format(outdir=config["outdir"]),
        tbi  = "{outdir}/variant/gatk_all.vcf.gz.tbi".format(outdir=config["outdir"]),
    params:
        mlist = expand(" -I {outdir}/variant/gatk_{mychr}_genotyped.vcf.gz",outdir=config["outdir"],mychr=CHRS),
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx16G' GatherVcfs {params.mlist} -O {output.vcf}
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx16G' IndexFeatureFile -I {output.vcf}
        #-I vcf_list_to_merge.txt
        """


# -------------------- SNPs selection and filtering --------------------------------
# 3-7) select SNPs only
rule gatk4_select_snps_variants:
    input:
        vcf = "{outdir}/variant/gatk_all.vcf.gz".format(outdir=config["outdir"]),
        tbi = "{outdir}/variant/gatk_all.vcf.gz.tbi".format(outdir=config["outdir"]),
    output:
        vcf="{outdir}/variant/gatk_all.raw_snps.vcf.gz".format(outdir=config["outdir"]),
        tbi="{outdir}/variant/gatk_all.raw_snps.vcf.gz.tbi".format(outdir=config["outdir"]),
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 1
    message: "GATK4 select SNPs (only) variants vcf\n"
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx12G -XX:ParallelGCThreads={threads}' SelectVariants \
        -select-type SNP \
        --variant {input.vcf} \
        --reference {params.ref} \
        --output {output.vcf}
        """

# 3-8) hard filtering as outlined in GATK docs
# (https://gatkforums.broadinstitute.org/gatk/discussion/2806/howto-apply-hard-filters-to-a-call-set)
# https://gatk.broadinstitute.org/hc/en-us/articles/360035890471-Hard-filtering-germline-short-variants
# QualByDepth (QD)
# FisherStrand (FS)
# StrandOddsRatio (SOR)
# RMSMappingQuality (MQ)
# MappingQualityRankSumTest (MQRankSum)
# ReadPosRankSumTest (ReadPosRankSum)
#
# https://gatk.broadinstitute.org/hc/en-us/articles/360035531112?id=2806
# For reference, here are some basic filtering thresholds to improve upon.
# QD < 2.0
# QUAL < 30.0
# SOR > 3.0
# FS > 60.0
# MQ < 40.0
# MQRankSum < -12.5
# ReadPosRankSum < -8.0
rule gatk4_filterSnps:
    input:
        vcf="{outdir}/variant/gatk_all.raw_snps.vcf.gz".format(outdir=config["outdir"]),
        tbi="{outdir}/variant/gatk_all.raw_snps.vcf.gz.tbi".format(outdir=config["outdir"]),
    output:
        vcf="{outdir}/variant/gatk_all.filtered_snps.vcf.gz".format(outdir=config["outdir"]),
        tbi="{outdir}/variant/gatk_all.filtered_snps.vcf.gz.tbi".format(outdir=config["outdir"]),
    params:
        vcftmp ="{outdir}/variant/vcf.snp.tmp.vcf.gz".format(outdir=config["outdir"]),
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"],
        QUAL_filter = config["snp_QUAL_filter"],
        QD_filter = config["snp_QD_filter"],
        FS_filter = config["snp_FS_filter"],
        MQ_filter = config["snp_MQ_filter"],
        SOR_filter = config["snp_SOR_filter"],
        MQRankSum_filter = config["snp_MQRankSum_filter"],
        ReadPosRankSum_filter = config["snp_ReadPosRankSum_filter"]
    threads: 1
    message: "GATK4 select SNPs (only) variants vcf\n"
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx12G -XX:ParallelGCThreads={threads}' VariantFiltration \
        --variant {input.vcf} \
        --reference {params.ref} \
        --output {params.vcftmp} \
        -filter-name "QUAL_filter" -filter "QUAL {params.QUAL_filter}" \
        -filter-name "QD_filter" -filter "QD {params.QD_filter}" \
        -filter-name "FS_filter" -filter "FS {params.FS_filter}" \
        -filter-name "MQ_filter" -filter "MQ {params.MQ_filter}" \
        -filter-name "SOR_filter" -filter "SOR {params.SOR_filter}" \
        -filter-name "MQRankSum_filter" -filter "MQRankSum {params.MQRankSum_filter}" \
        --genotype-filter-expression "DP < 3" \
        --genotype-filter-name "LowDP3" \
        -filter-name "ReadPosRankSum_filter" -filter "ReadPosRankSum {params.ReadPosRankSum_filter}"

        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx12G -XX:ParallelGCThreads={threads}' SelectVariants \
        --variant {params.vcftmp} \
        --set-filtered-gt-to-nocall \
        --exclude-filtered \
        --reference {params.ref} \
        --output {output.vcf}
        rm -f {params.vcftmp} {params.vcftmp}.tbi
        """

# 3-9) rules for creating quality score graphs for raw SNPs.
# First we do a table with the quality score using the VariantsToTable option
rule gatk4_snps_raw_score_table:
    input:
        vcf="{outdir}/variant/gatk_all.raw_snps.vcf.gz".format(outdir=config["outdir"])
    output:
        csv="{outdir}/variant/gatk_all.score_raw_snps.csv".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 1
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx12G -XX:ParallelGCThreads={threads}' VariantsToTable \
        -V {input.vcf} \
        -R {params.ref} \
        -F CHROM -F POS -F QUAL -F QD -F DP -F MQ -F MQRankSum -F FS -F ReadPosRankSum -F SOR \
        -O {output.csv}
        """

# 3-10) rules for creating quality score graphs for filtered SNPs.
# First we do a table with the quality score using the VariantsToTable option
rule gatk4_snps_filtered_score_table:
    input:
        vcf="{outdir}/variant/gatk_all.filtered_snps.vcf.gz".format(outdir=config["outdir"])
    output:
        csv="{outdir}/variant/gatk_all.score_filtered_snps.csv".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 1
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx12G -XX:ParallelGCThreads={threads}' VariantsToTable \
        -V {input.vcf} \
        -R {params.ref} \
        -F CHROM -F POS -F QUAL -F QD -F DP -F MQ -F MQRankSum -F FS -F ReadPosRankSum -F SOR \
        -O {output.csv}
        """

## 3-11) plots run Rscript to create quality score graphs to determine filters for indels vcf and snps vcf
## Rscript "graph_score_qual.R" must be download
rule gatk4_snps_quality_graphe_pdf:
    input:
        rawsnps="{outdir}/variant/gatk_all.score_raw_snps.csv".format(outdir=config["outdir"]),
        filteredsnps = "{outdir}/variant/gatk_all.score_filtered_snps.csv".format(outdir=config["outdir"])
    output:
        pdf="{outdir}/variant/gatk_all.score_snps.pdf".format(outdir=config["outdir"])
    params:
        bind = config["BIND"],
        R_bin = config["R_bin"]
    shell:
        """
        singularity exec {params.bind} {params.R_bin} \
        Rscript --vanilla gatk_scores_qual_raw_vs_filtered.R {input.rawsnps} {input.filteredsnps} "SNPs" {output.pdf}
        """


# ----------------------------- INDELs selection and filtering ------------------------------------
# 3-12) select INDELs only
rule gatk4_select_indels_variants:
    input:
        vcf = "{outdir}/variant/gatk_all.vcf.gz".format(outdir=config["outdir"])
    output:
        vcf="{outdir}/variant/gatk_all.raw_indels.vcf.gz".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 1
    message: "GATK4 select INDELs (only) variants vcf\n"
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx12G -XX:ParallelGCThreads={threads}' SelectVariants \
        -select-type INDEL \
        --variant {input.vcf} \
        --reference {params.ref} \
        --output {output.vcf}
        """
# 3-13) hard filtering as outlined in GATK docs
# (https://gatkforums.broadinstitute.org/gatk/discussion/2806/howto-apply-hard-filters-to-a-call-set)
# (https://gatk.broadinstitute.org/hc/en-us/articles/360035890471)
# "QD < 2.0 || FS > 200.0 || ReadPosRankSum < -20.0"
rule gatk4_filterIndels:
    input:
        vcf="{outdir}/variant/gatk_all.raw_indels.vcf.gz".format(outdir=config["outdir"])
    output:
        vcf="{outdir}/variant/gatk_all.filtered_indels.vcf.gz".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"],
        QUAL_filter = config["indel_QUAL_filter"],
        QD_filter = config["indel_QD_filter"],
        FS_filter = config["indel_FS_filter"],
        ReadPosRankSum_filter = config["indel_ReadPosRankSum_filter"]
    threads: 1
    message: "GATK4 select INDELs (only) variants vcf\n"
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx12G -XX:ParallelGCThreads={threads}' VariantFiltration \
        --variant {input.vcf} \
        --reference {params.ref} \
        --output {output.vcf} \
        -filter-name "QUAL_filter" -filter "QUAL {params.QUAL_filter}" \
        -filter-name "QD_filter" -filter "QD {params.QD_filter}" \
        -filter-name "FS_filter" -filter "FS {params.FS_filter}" \
        -filter-name "ReadPosRankSum_filter" -filter "ReadPosRankSum {params.ReadPosRankSum_filter}"
        """

# 3-14) rules for creating quality score graphs for raw SNPs.
# First we do a table with the quality score using the VariantsToTable option
rule gatk4_indels_raw_score_table:
    input:
        vcf="{outdir}/variant/gatk_all.raw_indels.vcf.gz".format(outdir=config["outdir"])
    output:
        csv="{outdir}/variant/gatk_all.score_raw_indels.csv".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 1
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx12G -XX:ParallelGCThreads={threads}' VariantsToTable \
        -V {input.vcf} \
        -R {params.ref} \
        -F CHROM -F POS -F QUAL -F QD -F DP -F MQ -F MQRankSum -F FS -F ReadPosRankSum -F SOR \
        -O {output.csv}
        """

# 3-15) rules for creating quality score graphs for filtered SNPs.
# First we do a table with the quality score using the VariantsToTable option
rule gatk4_indels_filtered_score_table:
    input:
        vcf="{outdir}/variant/gatk_all.filtered_indels.vcf.gz".format(outdir=config["outdir"])
    output:
        csv="{outdir}/variant/gatk_all.score_filtered_indels.csv".format(outdir=config["outdir"])
    params:
        ref = config["REFPATH"] + "/" + config["GENOME"],
        bind = config["BIND"],
        samtools_bin = config["samtools_bin"],
        gatk4_bin = config["gatk4_bin"]
    threads: 1
    shell:
        """
        singularity exec {params.bind} {params.gatk4_bin} \
        gatk --java-options '-Xmx12G -XX:ParallelGCThreads={threads}' VariantsToTable \
        -V {input.vcf} \
        -R {params.ref} \
        -F CHROM -F POS -F QUAL -F QD -F DP -F MQ -F MQRankSum -F FS -F ReadPosRankSum -F SOR \
        -O {output.csv}
        """

## 3-16) plots run Rscript to create quality score graphs to determine filters for indels vcf and snps vcf
## Rscript "graph_score_qual.R" must be download
rule gatk4_indels_quality_graphe_pdf:
    input:
        rawindels="{outdir}/variant/gatk_all.score_raw_indels.csv".format(outdir=config["outdir"]),
        filteredindels = "{outdir}/variant/gatk_all.score_filtered_indels.csv".format(outdir=config["outdir"])
    output:
        pdf="{outdir}/variant/gatk_all.score_indels.pdf".format(outdir=config["outdir"])
    params:
        bind = config["BIND"],
        R_bin = config["R_bin"]
    shell:
        """
        singularity exec {params.bind} {params.R_bin} \
        Rscript --vanilla gatk_scores_qual_raw_vs_filtered.R {input.rawindels} {input.filteredindels} "INDELs" {output.pdf}
        """


# recalibration need vcf reference
#rule gatk4_recalibrate_calls:

#to compress & index a vcf:
#bgzip file.vcf       # or:   bcftools view file.vcf -Oz -o file.vcf.gz
#tabix file.vcf.gz    # or:   bcftools index file.vcf.gz

######################################## END GATK #################################################33

