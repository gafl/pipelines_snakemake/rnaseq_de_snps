# RNASeq pipeline for Paired-end Illumina sequencing
## From raw fastq data to genes count table.
### Pipeline features:
### 1) read preprocessing,
### 2) reads QC,
### 3) mapping with STAR using 2pass method,
### 4) mapping QC
### 5) genes count table
### 6) parallelized (by ref sequence) SNPs calling based on GATK cf:  https://gatk.broadinstitute.org/hc/en-us/articles/360035531192-RNAseq-short-variant-discovery-SNPs-Indels-
### Counts table directly usable in the dicoexpress R pipeline.
## Snakemake features: fastq from csv file, config, modules, SLURM
### added a extra column for grouping samples (cf: samples.csv)
### added merge bam rule & function getit in utils.smk
### Workflow steps are descibed in the dag_rules.pdf
### Snakemake rules are based on [Snakemake modules](https://forgemia.inra.fr/gafl/snakemake_modules)


### Files description:
### 1) Snakefile
    - Snakefile.smk  self-contained snakefile (all rules are included)
    - Snakefile_modules.smk uses external rules (include directive)

### 2) Configuration file in yaml format:, paths, singularity images paths, parameters,....
    - config.yaml

### 3) a sbatch file to run the pipeline: (to be edited)
    - run_snakemake_pipeline.slurm

### 4) A slurm directive (#core, mem,...) in json format. Can be adjusted if needed
    - cluster.json

### 5) samples file in csv format
    Must contens at least 2 columns for SE reads and 3 for PE reads (tab separator )
    SampleName  fq1     fq2
    SampleName : your sample ID
    fq1: fastq file for a given sample
    fq2: read 2 for paired-end reads

    - samples.csv


## RUN:

### 1) Optionaly If using external rules/modules get all modules from the git:
`git clone https://forgemia.inra.fr/gafl/snakemake_modules.git smkmodules`

### 2) edit the config.yaml

### 3) set your samples in the sample.csv

### 4) adjust the run_snakemake_pipeline.slurm file

### 5) run pipelene:
`sbatch run_snakemake_pipeline.slurm`


### [Dicoexpress gitlab MIA](https://forgemia.inra.fr/GNet/dicoexpress)


#### Documentation being written (still)


